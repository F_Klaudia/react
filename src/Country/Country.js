import React from 'react';
import ireland from '../images/ireland.jpeg';
import czechia from '../images/czechia.jpeg';
import china from '../images/china.jpeg';
import './Country.css';


const country=(props)=>{


let countryImg;

if(props.chosen==='Ireland'){
  countryImg="https://images.pexels.com/photos/422844/dublin-famous-colorful-doors-422844.jpeg?auto=compress&cs=tinysrgb&h=650&w=940";
}else if(props.chosen==='China'){
  countryImg="https://images.pexels.com/photos/19885/pexels-photo.jpg?auto=compress&cs=tinysrgb&h=650&w=940";
}else if(props.chosen==='Czech Republic'){
  countryImg="https://images.pexels.com/photos/771023/pexels-photo-771023.jpeg?auto=compress&cs=tinysrgb&h=650&w=940";
}else{
  countryImg="";
}


return(

<div>
  <img src={countryImg} alt='Choose your favourite country from the list'/>
  </div>
);




};

export default country;
