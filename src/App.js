import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import 'jquery';
import 'bootstrap/dist/js/bootstrap';
import Country from './Country/Country';


class App extends Component {

state={

choose:''

}

      onSelectedHandler = (event) => {

        this.setState({choose:event.target.value})

      }



  render() {


    return (

  <div className="container-fuid">


      <div className="row">
        <div className="col-sm-6 col-sm-offset-6">
            <h4> Providing houses worldwide</h4>
        </div>
      </div>

      <div className="row">
        <div className="col-sm-6 col-sm-offset-6">
          <h5>Look for your dream house in country:</h5>
        </div>
      </div>

      <div className="select_drop">
        <select className="form-control" onChange={this.onSelectedHandler} >
          <option value="empty">  </option>
          <option value="Ireland">Ireland</option>
          <option value="Czech Republic">Czech Republic</option>
          <option value="China">China</option>
        </select>
      </div>



  <Country chosen={this.state.choose}/>


</div>


    );
  }
}

export default App;
